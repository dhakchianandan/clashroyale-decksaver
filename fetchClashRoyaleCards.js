const path = require("path");
const download = require("image-downloader");

const clashRoyaleCards = [
  {
    _id: 26000000,
    name: "Knight",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/jAj1Q5rclXxU9kVImGqSJxa4wEMfEhvwNQ_4jiGUuqg.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/knight.png",
    elixir: "3",
  },
  {
    _id: 26000001,
    name: "Archers",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/W4Hmp8MTSdXANN8KdblbtHwtsbt0o749BbxNqmJYfA8.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/archers.png",
    elixir: "3",
  },
  {
    _id: 26000002,
    name: "Goblins",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/X_DQUye_OaS3QN6VC9CPw05Fit7wvSm3XegXIXKP--0.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/goblins.png",
    elixir: "2",
  },
  {
    _id: 26000003,
    name: "Giant",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Axr4ox5_b7edmLsoHxBX3vmgijAIibuF6RImTbqLlXE.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/giant.png",
    elixir: "5",
  },
  {
    _id: 26000004,
    name: "P.E.K.K.A",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/MlArURKhn_zWAZY-Xj1qIRKLVKquarG25BXDjUQajNs.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/pekka.png",
    elixir: "7",
  },
  {
    _id: 26000005,
    name: "Minions",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/yHGpoEnmUWPGV_hBbhn-Kk-Bs838OjGzWzJJlQpQKQA.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/minion.png",
    elixir: "3",
  },
  {
    _id: 26000006,
    name: "Balloon",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/qBipxLo-3hhCnPrApp2Nn3b2NgrSrvwzWytvREev0CY.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/chr_balloon.png",
    elixir: "5",
  },
  {
    _id: 26000007,
    name: "Witch",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/cfwk1vzehVyHC-uloEIH6NOI0hOdofCutR5PyhIgO6w.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/chr_witch.png",
    elixir: "5",
  },
  {
    _id: 26000008,
    name: "Barbarians",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/TvJsuu2S4yhyk1jVYUAQwdKOnW4U77KuWWOTPOWnwfI.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/barbarians.png",
    elixir: "5",
  },
  {
    _id: 26000009,
    name: "Golem",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/npdmCnET7jmVjJvjJQkFnNSNnDxYHDBigbvIAloFMds.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/chr_golem.png",
    elixir: "8",
  },
  {
    _id: 26000010,
    name: "Skeletons",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/oO7iKMU5m0cdxhYPZA3nWQiAUh2yoGgdThLWB1rVSec.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/skeletons.png",
    elixir: "1",
  },
  {
    _id: 26000011,
    name: "Valkyrie",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/0lIoYf3Y_plFTzo95zZL93JVxpfb3MMgFDDhgSDGU9A.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/valkyrie.png",
    elixir: "4",
  },
  {
    _id: 26000012,
    name: "Skeleton Army",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/fAOToOi1pRy7svN2xQS6mDkhQw2pj9m_17FauaNqyl4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/skeleton_horde.png",
    elixir: "3",
  },
  {
    _id: 26000013,
    name: "Bomber",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/12n1CesxKIcqVYntjxcF36EFA-ONw7Z-DoL0_rQrbdo.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/bomber.png",
    elixir: "3",
  },
  {
    _id: 26000014,
    name: "Musketeer",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Tex1C48UTq9FKtAX-3tzG0FJmc9jzncUZG3bb5Vf-Ds.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/musketeer.png",
    elixir: "4",
  },
  {
    _id: 26000015,
    name: "Baby Dragon",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/cjC9n4AvEZJ3urkVh-rwBkJ-aRSsydIMqSAV48hAih0.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/baby_dragon.png",
    elixir: "4",
  },
  {
    _id: 26000016,
    name: "Prince",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/3JntJV62aY0G1Qh6LIs-ek-0ayeYFY3VItpG7cb9I60.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/prince.png",
    elixir: "5",
  },
  {
    _id: 26000017,
    name: "Wizard",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Mej7vnv4H_3p_8qPs_N6_GKahy6HDr7pU7i9eTHS84U.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/wizard.png",
    elixir: "5",
  },
  {
    _id: 26000018,
    name: "Mini P.E.K.K.A",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Fmltc4j3Ve9vO_xhHHPEO3PRP3SmU2oKp2zkZQHRZT4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/mini_pekka.png",
    elixir: "4",
  },
  {
    _id: 26000019,
    name: "Spear Goblins",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/FSDFotjaXidI4ku_WFpVCTWS1hKGnFh1sxX0lxM43_E.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/goblin_archer.png",
    elixir: "2",
  },
  {
    _id: 26000020,
    name: "Giant Skeleton",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/0p0gd0XaVRu1Hb1iSG1hTYbz2AN6aEiZnhaAib5O8Z8.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/giant_skeleton.png",
    elixir: "6",
  },
  {
    _id: 26000021,
    name: "Hog Rider",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Ubu0oUl8tZkusnkZf8Xv9Vno5IO29Y-jbZ4fhoNJ5oc.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/hog_rider.png",
    elixir: "4",
  },
  {
    _id: 26000022,
    name: "Minion Horde",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Wyjq5l0IXHTkX9Rmpap6HaH08MvjbxFp1xBO9a47YSI.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/minion_horde.png",
    elixir: "5",
  },
  {
    _id: 26000023,
    name: "Ice Wizard",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/W3dkw0HTw9n1jB-zbknY2w3wHuyuLxSRIAV5fUT1SEY.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/ice_wizard.png",
    elixir: "3",
  },
  {
    _id: 26000024,
    name: "Royal Giant",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/mnlRaNtmfpQx2e6mp70sLd0ND-pKPF70Cf87_agEKg4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/royal_giant.png",
    elixir: "6",
  },
  {
    _id: 26000025,
    name: "Guards",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/1ArKfLJxYo6_NU_S9cAeIrfbXqWH0oULVJXedxBXQlU.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/skeleton_warriors.png",
    elixir: "3",
  },
  {
    _id: 26000026,
    name: "Princess",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/bAwMcqp9EKVIKH3ZLm_m0MqZFSG72zG-vKxpx8aKoVs.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/princess.png",
    elixir: "3",
  },
  {
    _id: 26000027,
    name: "Dark Prince",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/M7fXlrKXHu2IvpSGpk36kXVstslbR08Bbxcy0jQcln8.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/dark_prince.png",
    elixir: "4",
  },
  {
    _id: 26000028,
    name: "Three Musketeers",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/_J2GhbkX3vswaFk1wG-dopwiHyNc_YiPhwroiKF3Mek.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/three_musketeers.png",
    elixir: "9",
  },
  {
    _id: 26000029,
    name: "Lava Hound",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/unicRQ975sBY2oLtfgZbAI56ZvaWz7azj-vXTLxc0r8.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/lava_hound.png",
    elixir: "7",
  },
  {
    _id: 26000030,
    name: "Ice Spirit",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/lv1budiafU9XmSdrDkk0NYyqASAFYyZ06CPysXKZXlA.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/snow_spirits.png",
    elixir: "1",
  },
  {
    _id: 26000031,
    name: "Fire Spirits",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/16-BqusVvynIgYI8_Jci3LDC-r8AI_xaIYLgXqtlmS8.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/fire_spirits.png",
    elixir: "2",
  },
  {
    _id: 26000032,
    name: "Miner",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Y4yWvdwBCg2FpAZgs8T09Gy34WOwpLZW-ttL52Ae8NE.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/miner.png",
    elixir: "3",
  },
  {
    _id: 26000033,
    name: "Sparky",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/2GKMkBrArZXgQxf2ygFjDs4VvGYPbx8F6Lj_68iVhIM.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/zapMachine.png",
    elixir: "6",
  },
  {
    _id: 26000034,
    name: "Bowler",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/SU4qFXmbQXWjvASxVI6z9IJuTYolx4A0MKK90sTIE88.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/bowler.png",
    elixir: "5",
  },
  {
    _id: 26000035,
    name: "Lumberjack",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/E6RWrnCuk13xMX5OE1EQtLEKTZQV6B78d00y8PlXt6Q.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/rage_barbarian.png",
    elixir: "4",
  },
  {
    _id: 26000036,
    name: "Battle Ram",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/dyc50V2cplKi4H7pq1B3I36pl_sEH5DQrNHboS_dbbM.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/battle_ram.png",
    elixir: "4",
  },
  {
    _id: 26000037,
    name: "Inferno Dragon",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/y5HDbKtTbWG6En6TGWU0xoVIGs1-iQpIP4HC-VM7u8A.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/inferno_dragon.png",
    elixir: "4",
  },
  {
    _id: 26000038,
    name: "Ice Golem",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/r05cmpwV1o7i7FHodtZwW3fmjbXCW34IJCsDEV5cZC4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/ice_golem.png",
    elixir: "2",
  },
  {
    _id: 26000039,
    name: "Mega Minion",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/-T_e4YLbuhPBKbYnBwQfXgynNpp5eOIN_0RracYwL9c.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/mega_minion.png",
    elixir: "3",
  },
  {
    _id: 26000040,
    name: "Dart Goblin",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/BmpK3bqEAviflqHCdxxnfm-_l3pRPJw3qxHkwS55nCY.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/blowdart_goblin.png",
    elixir: "3",
  },
  {
    _id: 26000041,
    name: "Goblin Gang",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/NHflxzVAQT4oAz7eDfdueqpictb5vrWezn1nuqFhE4w.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/goblin_gang.png",
    elixir: "3",
  },
  {
    _id: 26000042,
    name: "Electro Wizard",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/RsFaHgB3w6vXsTjXdPr3x8l_GbV9TbOUCvIx07prbrQ.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/electro_wizard.png",
    elixir: "4",
  },
  {
    _id: 26000043,
    name: "Elite Barbarians",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/C88C5JH_F3lLZj6K-tLcMo5DPjrFmvzIb1R2M6xCfTE.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/angry_barbarian.png",
    elixir: "6",
  },
  {
    _id: 26000044,
    name: "Hunter",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/VNabB1WKnYtYRSG7X_FZfnZjQDHTBs9A96OGMFmecrA.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/hunter.png",
    elixir: "4",
  },
  {
    _id: 26000045,
    name: "Executioner",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/9XL5BP2mqzV8kza6KF8rOxrpCZTyuGLp2l413DTjEoM.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/executioner.png",
    elixir: "5",
  },
  {
    _id: 26000046,
    name: "Bandit",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/QWDdXMKJNpv0go-HYaWQWP6p8uIOHjqn-zX7G0p3DyM.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/bandit.png",
    elixir: "3",
  },
  {
    _id: 26000047,
    name: "Royal Recruits",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/jcNyYGUiXXNz3kuz8NBkHNKNREQKraXlb_Ts7rhCIdM.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/royal_recruits.png",
    elixir: "7",
  },
  {
    _id: 26000048,
    name: "Night Witch",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/NpCrXDEDBBJgNv9QrBAcJmmMFbS7pe3KCY8xJ5VB18A.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/dark_witch.png",
    elixir: "4",
  },
  {
    _id: 26000049,
    name: "Bats",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/EnIcvO21hxiNpoI-zO6MDjLmzwPbq8Z4JPo2OKoVUjU.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/bats.png",
    elixir: "2",
  },
  {
    _id: 26000050,
    name: "Royal Ghost",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/3En2cz0ISQAaMTHY3hj3rTveFN2kJYq-H4VxvdJNvCM.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/ghost.png",
    elixir: "3",
  },
  {
    _id: 26000051,
    name: "Ram Rider",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/QaJyerT7f7oMyZ3Fv1glKymtLSvx7YUXisAulxl7zRI.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/ram_rider.png",
    elixir: "5",
  },
  {
    _id: 26000052,
    name: "Zappies",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/QZfHRpLRmutZbCr5fpLnTpIp89vLI6NrAwzGZ8tHEc4.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/zappies.png",
    elixir: "4",
  },
  {
    _id: 26000053,
    name: "Rascals",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/KV48DfwVHKx9XCjzBdk3daT_Eb52Me4VgjVO7WctRc4.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/rascals.png",
    elixir: "5",
  },
  {
    _id: 26000054,
    name: "Cannon Cart",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/aqwxRz8HXzqlMCO4WMXNA1txynjXTsLinknqsgZLbok.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/cannon_cart.png",
    elixir: "5",
  },
  {
    _id: 26000055,
    name: "Mega Knight",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/O2NycChSNhn_UK9nqBXUhhC_lILkiANzPuJjtjoz0CE.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/mega_knight.png",
    elixir: "7",
  },
  {
    _id: 26000056,
    name: "Skeleton Barrel",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/vCB4DWCcrGbTkarjcOiVz4aNDx6GWLm0yUepg9E1MGo.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/skeleton_balloon.png",
    elixir: "3",
  },
  {
    _id: 26000057,
    name: "Flying Machine",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/hzKNE3QwFcrSrDDRuVW3QY_OnrDPijSiIp-PsWgFevE.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/flying_machine.png",
    elixir: "4",
  },
  {
    _id: 26000058,
    name: "Wall Breakers",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/_xPphEfC8eEwFNrfU3cMQG9-f5JaLQ31ARCA7l3XtW4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/wallbreaker.png",
    elixir: "2",
  },
  {
    _id: 26000059,
    name: "Royal Hogs",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/ASSQJG_MoVq9e81HZzo4bynMnyLNpNJMfSLb3hqydOw.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/royal_hog.png",
    elixir: "5",
  },
  {
    _id: 26000060,
    name: "Goblin Giant",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/SoW16cY3jXBwaTDvb39DkqiVsoFVaDWbzf5QBYphJrY.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/goblin_giant.png",
    elixir: "6",
  },
  {
    _id: 26000061,
    name: "Fisherman",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/U2KZ3g0wyufcuA5P2Xrn3Z3lr1WiJmc5S0IWOZHgizQ.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/fisherman.png",
    elixir: "3",
  },
  {
    _id: 26000062,
    name: "Magic Archer",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Avli3W7BxU9HQ2SoLiXnBgGx25FoNXUSFm7OcAk68ek.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/magic_archer.png",
    elixir: "4",
  },
  {
    _id: 26000063,
    name: "Electro Dragon",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/tN9h6lnMNPCNsx0LMFmvpHgznbDZ1fBRkx-C7UfNmfY.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/electro_dragon.png",
    elixir: "5",
  },
  {
    _id: 26000064,
    name: "Firecracker",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/c1rL3LO1U2D9-TkeFfAC18gP3AO8ztSwrcHMZplwL2Q.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/firecracker.png",
    elixir: "3",
  },
  {
    _id: 26000067,
    name: "Elixir Golem",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/puhMsZjCIqy21HW3hYxjrk_xt8NIPyFqjRy-BeLKZwo.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/elixir_golem.png",
    elixir: "3",
  },
  {
    _id: 26000068,
    name: "Battle Healer",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/KdwXcoigS2Kg-cgA7BJJIANbUJG6SNgjetRQ-MegZ08.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/battle_healer.png",
    elixir: "4",
  },
  {
    _id: 26000080,
    name: "Skeleton Dragons",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/qPOtg9uONh47_NLxGhhFc_ww9PlZ6z3Ry507q1NZUXs.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/skeletondragon.png",
    elixir: "4",
  },
  {
    _id: 26000083,
    name: "Mother Witch",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/fO-Xah8XZkYKaSK9SCp3wnzwxtvIhun9NVY-zzte1Ng.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/motherwitch.png",
    elixir: "4",
  },
  {
    _id: 26000084,
    name: "Electro Spirit",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/WKd4-IAFsgPpMo7dDi9sujmYjRhOMEWiE07OUJpvD9g.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/electrospirit.png",
    elixir: "1",
  },
  {
    _id: 26000085,
    name: "Electro Giant",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/_uChZkNHAMq6tPb3v6A49xinOe3CnhjstOhG6OZbPYc.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/electrogiant.png",
    elixir: "8",
  },
  {
    _id: 27000000,
    name: "Cannon",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/nZK1y-beLxO5vnlyUhK6-2zH2NzXJwqykcosqQ1cmZ8.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/chaos_cannon.png",
    elixir: "3",
  },
  {
    _id: 27000001,
    name: "Goblin Hut",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/l8ZdzzNLcwB4u7ihGgxNFQOjCT_njFuAhZr7D6PRF7E.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/fire_furnace.png",
    elixir: "5",
  },
  {
    _id: 27000002,
    name: "Mortar",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/lPOSw6H7YOHq2miSCrf7ZDL3ANjhJdPPDYOTujdNrVE.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/building_mortar.png",
    elixir: "4",
  },
  {
    _id: 27000003,
    name: "Inferno Tower",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/GSHY_wrooMMLET6bG_WJB8redtwx66c4i80ipi4gYOM.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/building_inferno.png",
    elixir: "5",
  },
  {
    _id: 27000004,
    name: "Bomb Tower",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/rirYRyHPc97emRjoH-c1O8uZCBzPVnToaGuNGusF3TQ.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/bomb_tower.png",
    elixir: "4",
  },
  {
    _id: 27000005,
    name: "Barbarian Hut",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/ho0nOG2y3Ch86elHHcocQs8Fv_QNe0cFJ2CijsxABZA.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/barbarian_hut.png",
    elixir: "7",
  },
  {
    _id: 27000006,
    name: "Tesla",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/OiwnGrxFMNiHetYEerE-UZt0L_uYNzFY7qV_CA_OxR4.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/building_tesla.png",
    elixir: "4",
  },
  {
    _id: 27000007,
    name: "Elixir Collector",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/BGLo3Grsp81c72EpxLLk-Sofk3VY56zahnUNOv3JcT0.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/building_elixir_collector.png",
    elixir: "6",
  },
  {
    _id: 27000008,
    name: "X-Bow",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/zVQ9Hme1hlj9Dc6e1ORl9xWwglcSrP7ejow5mAhLUJc.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/building_xbow.png",
    elixir: "6",
  },
  {
    _id: 27000009,
    name: "Tombstone",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/LjSfSbwQfkZuRJY4pVxKspZ-a0iM5KAhU8w-a_N5Z7Y.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/tombstone.png",
    elixir: "3",
  },
  {
    _id: 27000010,
    name: "Furnace",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/iqbDiG7yYRIzvCPXdt9zPb3IvMt7F7Gi4wIPnh2x4aI.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/firespirit_hut.png",
    elixir: "4",
  },
  {
    _id: 27000012,
    name: "Goblin Cage",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/vD24bBgK4rSq7wx5QEbuqChtPMRFviL_ep76GwQw1yA.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/goblin_cage.png",
    elixir: "4",
  },
  {
    _id: 28000000,
    name: "Fireball",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/lZD9MILQv7O-P3XBr_xOLS5idwuz3_7Ws9G60U36yhc.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/fire_fireball.png",
    elixir: "4",
  },
  {
    _id: 28000001,
    name: "Arrows",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Flsoci-Y6y8ZFVi5uRFTmgkPnCmMyMVrU7YmmuPvSBo.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/order_volley.png",
    elixir: "3",
  },
  {
    _id: 28000002,
    name: "Rage",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/bGP21OOmcpHMJ5ZA79bHVV2D-NzPtDkvBskCNJb7pg0.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/rage.png",
    elixir: "2",
  },
  {
    _id: 28000003,
    name: "Rocket",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Ie07nQNK9CjhKOa4-arFAewi4EroqaA-86Xo7r5tx94.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/rocket.png",
    elixir: "6",
  },
  {
    _id: 28000004,
    name: "Goblin Barrel",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/CoZdp5PpsTH858l212lAMeJxVJ0zxv9V-f5xC8Bvj5g.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/goblin_barrel.png",
    elixir: "3",
  },
  {
    _id: 28000005,
    name: "Freeze",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/I1M20_Zs_p_BS1NaNIVQjuMJkYI_1-ePtwYZahn0JXQ.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/freeze.png",
    elixir: "4",
  },
  {
    _id: 28000006,
    name: "Mirror",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/wC6Cm9rKLEOk72zTsukVwxewKIoO4ZcMJun54zCPWvA.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/mirror.png",
    elixir: "1",
  },
  {
    _id: 28000007,
    name: "Lightning",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/fpnESbYqe5GyZmaVVYe-SEu7tE0Kxh_HZyVigzvLjks.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/lightning.png",
    elixir: "6",
  },
  {
    _id: 28000008,
    name: "Zap",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/7dxh2-yCBy1x44GrBaL29vjqnEEeJXHEAlsi5g6D1eY.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/zap.png",
    elixir: "2",
  },
  {
    _id: 28000009,
    name: "Poison",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/98HDkG2189yOULcVG9jz2QbJKtfuhH21DIrIjkOjxI8.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/poison.png",
    elixir: "4",
  },
  {
    _id: 28000010,
    name: "Graveyard",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Icp8BIyyfBTj1ncCJS7mb82SY7TPV-MAE-J2L2R48DI.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/graveyard.png",
    elixir: "5",
  },
  {
    _id: 28000011,
    name: "The Log",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/_iDwuDLexHPFZ_x4_a0eP-rxCS6vwWgTs6DLauwwoaY.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/the_log.png",
    elixir: "2",
  },
  {
    _id: 28000012,
    name: "Tornado",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/QJB-QK1QJHdw4hjpAwVSyZBozc2ZWAR9pQ-SMUyKaT0.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/tornado.png",
    elixir: "3",
  },
  {
    _id: 28000013,
    name: "Clone",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/mHVCet-1TkwWq-pxVIU2ZWY9_2z7Z7wtP25ArEUsP_g.png",
    statsRoyaleUrl: "https://cdn.statsroyale.com/images/cards/full/copy.png",
    elixir: "3",
  },
  {
    _id: 28000014,
    name: "Earthquake",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/XeQXcrUu59C52DslyZVwCnbi4yamID-WxfVZLShgZmE.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/earthquake.png",
    elixir: "3",
  },
  {
    _id: 28000015,
    name: "Barbarian Barrel",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/Gb0G1yNy0i5cIGUHin8uoFWxqntNtRPhY_jeMXg7HnA.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/barb_barrel.png",
    elixir: "2",
  },
  {
    _id: 28000016,
    name: "Heal Spirit",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/GITl06sa2nGRLPvboyXbGEv5E3I-wAwn1Eqa5esggbc.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/healspirit.png",
    elixir: "1",
  },
  {
    _id: 28000017,
    name: "Giant Snowball",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/7MaJLa6hK9WN2_VIshuh5DIDfGwm0wEv98gXtAxLDPs.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/snowball.png",
    elixir: "2",
  },
  {
    _id: 28000018,
    name: "Royal Delivery",
    iconUrls:
      "https://api-assets.clashroyale.com/cards/300/LPg7AGjGI3_xmi7gLLgGC50yKM1jJ2teWkZfoHJcIZo.png",
    statsRoyaleUrl:
      "https://cdn.statsroyale.com/images/cards/full/royal_delivery.png",
    elixir: "3",
  },
];

const deckCardOptions = clashRoyaleCards.map((clashRoyaleCard) => {
  return {
    url: clashRoyaleCard.statsRoyaleUrl,
    dest: path.join(
      __dirname,
      "clashroyale-cards",
      `${clashRoyaleCard._id}.png`
    ),
    extractFilename: false,
  };
});

deckCardOptions.forEach((deckCardOption) => {
  download
    .image(deckCardOption)
    .then(({ filename }) => {
      console.log("Saved to", filename); // saved to /path/to/dest/photo
    })
    .catch((err) => console.error(err));
});
