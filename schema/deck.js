const mongoose = require("mongoose");

const Card = require("./card");

module.exports = mongoose.model(
  "Deck",
  {
    _id: Number,
    name: String,
    url: String,
    elixir: String,
    cards_id: String,
    user_id: String,
    imgur_url: String,
  },
  "player-decks"
);
