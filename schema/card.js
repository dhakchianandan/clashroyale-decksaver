const mongoose = require("mongoose");

module.exports = mongoose.model(
  "Card",
  {
    _id: Number,
    name: String,
    iconUrls: String,
    statsRoyaleUrl: String,
    elixir: String,
  },
  "clashroyale-cards"
);
