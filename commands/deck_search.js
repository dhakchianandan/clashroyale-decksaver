const chunk = require("lodash/chunk");
const { MessageEmbed } = require("discord.js");

const Deck = require("../schema/deck");

module.exports = {
  category: "Decks",
  description: "Search Saved Decks",
  callback: async ({ message, args }) => {
    const { channel } = message;
    const [term] = args;

    const webhooks = await channel.fetchWebhooks();

    let webhook = webhooks.find(
      (webhook) => webhook.name === process.env.WEBHOOK_NAME
    );

    if (webhook) {
    } else {
      webhook = await channel.createWebhook(process.env.WEBHOOK_NAME, {
        avatar: process.env.WEBHOOK_AVATAR,
      });
    }

    const decks = await Deck.find({
      user_id: message.author.id,
      name: {
        $regex: term,
        $options: "i",
      },
    });

    if (decks.length) {
      const embeds = [];

      decks.forEach((deck) => {
        const embed = new MessageEmbed()
          .setDescription(
            `
${deck.name} - ${deck._id}

<:elixir:801082138211647508> Average Elixir Cost: ${deck.elixir} [<:save:801082138421755934> Copy Deck](${deck.url})
    `
          )
          .setImage(`${deck.imgur_url}`);

        embeds.push(embed);
      });

      chunk(embeds, 5).forEach((deckEmbeds) => {
        webhook.send(deckEmbeds);
      });
    } else {
      message.reply("No Decks Found");
    }
  },
};
