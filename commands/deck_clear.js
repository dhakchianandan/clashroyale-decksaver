const Deck = require("../schema/deck");

module.exports = {
  category: "Decks",
  description: "Clear Saved Decks",
  callback: async ({ message }) => {
    const deletedDecks = await Deck.deleteMany({
      user_id: message.author.id,
    });

    message.reply(`Cleared ${deletedDecks.deletedCount} Decks`);
  },
};
