const Deck = require("../schema/deck");

module.exports = {
  category: "Decks",
  description: "Delete Saved Deck By Id",
  minArgs: 1,
  maxArgs: 1,
  expectedArgs: "deck_id",
  callback: async ({ message, args }) => {
    let [deckIdToDelete] = args;
    const deletedDeck = await Deck.findByIdAndRemove(deckIdToDelete);
    if (deletedDeck) {
      message.reply(`Deleted Deck: ${deletedDeck.name}`);
    } else {
      message.reply(`No Deck Exists with the Id: ${deckIdToDelete}`);
    }
  },
};
