const path = require("path");
const imgur = require("imgur");
const mergeImages = require("merge-images");
const { Canvas, Image } = require("canvas");
const { MessageAttachment } = require("discord.js");

const Card = require("../schema/card");
const Deck = require("../schema/deck");

module.exports = {
  category: "Decks",
  description: "Save Deck",
  minArgs: 2,
  maxArgs: 2,
  expectedArgs: "name deck_url",
  callback: async ({ message, args }) => {
    // https://link.clashroyale.com/deck/en?deck=28000001;26000006;26000008;26000037;26000029;26000032;26000080;28000008
    // 277 * 329
    // 250 * 300
    let [deckName, deckURL] = args;

    if (deckURL.startsWith("https://link.clashroyale.com/deck/en?deck=")) {
      console.log("Deck Name:", deckName);
      console.log("Deck URL:", deckURL);

      if (deckURL.indexOf("&id=") != -1) {
        deckURL = deckURL.slice(0, deckURL.indexOf("&id="));
      }

      let [_, deckCards] = deckURL.split("=");
      deckCards = deckCards.split(";").sort();

      const cards = await Card.find().where("_id").in(deckCards).exec();
      cards.sort((a, b) => a.elixir - b.elixir);
      deckCards = cards.map((card) => card._id);

      const deckExists = await Deck.findOne({
        cards_id: deckCards.join(";"),
        user_id: message.author.id,
      }).exec();

      console.log("Deck Exists:", deckExists);

      if (deckExists) {
        message.reply("Deck Already Exists");
        return;
      }

      let totalElixir = cards.reduce(
        (totalElixir, { elixir }) => totalElixir + Number.parseInt(elixir),
        0
      );

      let deckCardsImages = deckCards.map((deckCard, index) => {
        return {
          src: path.join(process.cwd(), "clashroyale-cards", `${deckCard}.png`),
          x: 250 * index,
          y: 0,
        };
      });

      mergeImages(deckCardsImages, {
        width: 250 * 8,
        height: 300,
        Canvas: Canvas,
        Image: Image,
      }).then(async (b64) => {
        imgur.setClientId(process.env.IMGUR_CLIENT_ID);

        imgur
          .uploadBase64(b64.replace(/^data:image\/png;base64,/, ""))
          .then(async (imgurImage) => {
            const { link } = imgurImage.data;

            const deck = new Deck({
              _id: Date.now(),
              name: deckName,
              url: `${deckURL.split("=").shift()}=${deckCards.join(";")}`,
              elixir: (totalElixir / 8).toFixed(1),
              cards_id: deckCards.join(";"),
              user_id: message.author.id,
              imgur_url: link,
            });
            await deck.save();

            message.reply("Deck Saved", new MessageAttachment(link));
          })
          .catch((error) => {
            console.log(error);
            message.reply("Please try again after sometime");
          });
      });
    } else {
      message.reply("URL Not Supported");
    }
  },
};
