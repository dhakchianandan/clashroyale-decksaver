module.exports = {
  category: "Utils",
  description: "Bulk Delete Messages Older than 2 Weeks",
  callback: ({ message }) => {
    message.channel.messages.fetch({ limit: 100 }).then((messages) => {
      console.log(`Total Messages: ${messages.size}`);
      message.channel.bulkDelete(messages, true).then((deletedMessages) => {
        console.log(`Messages Deleted: ${deletedMessages.size}`);
      });
    });
  },
};
