require("dotenv").config();

const Discord = require("discord.js");
const WOKCommands = require("wokcommands");

const client = new Discord.Client({
  partials: ["MESSAGE", "REACTION"],
});

client.on("ready", () => {
  new WOKCommands(client, {
    commandsDir: process.env.COMMANDS_DIRECTORY,
    showWarns: true,
  })
    .setMongoPath(process.env.MONGO_URL)
    .setDefaultPrefix(process.env.DEFAULT_PREFIX)
    .setCategorySettings([
      {
        name: "Utils",
        emoji: "✅",
      },
      {
        name: "Decks",
        emoji: "💼",
      },
      {
        name: "Configuration",
        hidden: true,
      },
    ]);

  console.log(`Logged in as ${client.user.tag}!`);
});

client.login(process.env.DISCORD_BOT_TOKEN);
